<?php

namespace App\Components;

use RedBeanPHP\R;

/***
 * Clase para obtener los datos del API JSON y cargarlos en la tabla "customer"
 *
 * Class CustomerData
 * @package App\Components
 */
class CustomerData
{
    private $_url;

    public function __construct($url)
    {
        $this->_url = $url;
    }

    public function load()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        $raw_customers = $this->normalizeData(json_decode($result));

        foreach ($raw_customers as $reg) {
            if (!R::findOne('customer', 'document = ?', [$reg['document']])) { //Evitar duplicados por documento
                $customer = R::dispense('customer');

                $customer->job_title = $reg['job_title'];
                $customer->email_address = $reg['email_address'];
                $customer->first_name = $reg['first_name'];
                $customer->last_name = $reg['last_name'];
                $customer->document = $reg['document'];
                $customer->phone = $reg['phone'];
                $customer->country = $reg['country'];
                $customer->state = $reg['state'];
                $customer->city = $reg['city'];

                R::store($customer);
            }
        }
    }

    private function normalizeData($data)
    {
        $final_data = [];

        foreach ($data->objects as $reg) {
            $final_data[] = [
                'job_title' => $reg->job_title,
                'email_address' => $reg->email,
                'first_name' => $reg->first_name,
                'last_name' => $reg->last_name,
                'document' => $reg->document,
                'phone' => $reg->phone_number,
                'country' => $reg->country,
                'state' => $reg->state,
                'city' => $reg->city
            ];
        }

        return $final_data;
    }
}