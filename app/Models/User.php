<?php

namespace App\Models;

use RedBeanPHP\SimpleModel;

class User extends SimpleModel
{
    public static function isLogged()
    {
        return !empty($_SESSION['user']);
    }
}