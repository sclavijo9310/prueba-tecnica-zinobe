<?php


namespace App\Controllers;

use App\Models\User;
use Rakit\Validation\Validator;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/***
 * Clase base para controladores
 *
 * Class Controller
 * @package App\Controllers
 */
class Controller
{
    protected $twig;
    protected $user;
    protected $auth_methods = [];
    protected $public_methods = [];
    private $_validator;

    public function __construct($user, Validator $validator)
    {
        $loader = new FilesystemLoader('app/Views');
        $this->twig = new Environment($loader);
        $this->user = $user;
        $this->_validator = $validator;
        $this->_validator->setMessages([
            'required' => 'Este campo es obligatorio',
            'email' => 'Debe ingresar una dirección de correo válida',
            'min' => 'Debe contener un mínimo de :min caracteres'
        ]);
    }

    public function __call($method, $args)
    {
        if (in_array($method, $this->auth_methods)) {
            if (!User::isLogged())
                $this->redirect('/');
        } elseif (!in_array($method, $this->public_methods)) {
            if (User::isLogged())
                $this->redirect('/');
        }

        return call_user_func_array([$this, $method], $args);
    }

    protected function render($view, $params = [])
    {
        $params = array_merge($params, [
            'is_user_logged' => User::isLogged()
        ]);

        echo $this->twig->render($view . '.twig', $params);
    }

    protected function redirect($url)
    {
        header('Location: ' . $url, true, 302);
        exit;
    }

    protected function responseJSON($data, $statusCode = 200)
    {
        if (is_array($data))
            $data = json_encode($data);

        $sapi_type = php_sapi_name();
        if (substr($sapi_type, 0, 3) == 'cgi')
            header("Status: $statusCode");
        else
            header("HTTP/1.1 $statusCode");
        header('Content-Type: application/json');
        echo $data;
        exit;
    }

    protected function get($name = null, $defaultValue = null)
    {
        return $name === null ? $_GET : (isset($_GET[$name]) ? $_GET[$name] : $defaultValue);
    }

    protected function post($name = null, $defaultValue = null)
    {
        return $name === null ? $_POST : (isset($_POST[$name]) ? $_POST[$name] : $defaultValue);
    }

    protected function validate($data, $rules)
    {
        return $this->_validator->validate($data, $rules);
    }
}
