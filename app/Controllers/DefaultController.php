<?php

namespace App\Controllers;

use App\Components\CustomerData;
use App\Models\User;
use RedBeanPHP\R;
use Spatie\Async\Pool;

/***
 * Controlador default, se usa para manejar todas las rutas
 *
 * Class DefaultController
 * @package App\Controllers
 */
class DefaultController extends Controller
{
    protected $auth_methods = [
        'users',
        'customers',
        'loadCustomers',
        'logout'
    ];
    protected $public_methods = [
        'index'
    ];

    public function index()
    {
        $this->redirect(User::isLogged() ? '/users' : 'login');
    }

    protected function users()
    {
        $search = $this->get('search');
        $condition = null;

        if ($search)
            $condition = 'name like :search or document like :search or email_address like :search or country like :search';

        $this->render('users', ['users' => R::findAll('user', $condition, [':search' => "%$search%"]), 'search' => $search]);
    }

    protected function login()
    {
        $this->render('login');
    }

    protected function postLogin()
    {
        $user = R::findOne('user', 'email_address = ?', [$this->post('email_address')]);

        $validation = $this->validate($this->post(), [
            'email_address' => ['required', 'email', function ($value) use ($user) {
                if (!$user)
                    return 'El usuario no existe';
                return true;
            }],
            'password' => ['required', function ($value) use ($user) {
                if ($user && !($user->password === sha1($value)))
                    return 'Las credenciales son incorrectas';
                return true;
            }]
        ]);

        if ($validation->fails())
            $this->responseJSON($validation->errors()->toArray(), 422);

        if ($this->get('validating', false))
            $this->responseJSON(['ok']);

        $_SESSION['user'] = $user;
        $this->redirect('/users');
    }

    protected function register()
    {
        $this->render('register');
    }

    protected function postRegister()
    {
        $validation = $this->validate($this->post(), [
            'name' => ['required', 'min:3'],
            'document' => ['required', function ($value) {
                if (R::findOne('user', 'document = ?', [$value]))
                    return 'Este documento ya se encuentra registrado';
                return true;
            }],
            'email_address' => [function ($value) {
                if (R::findOne('user', 'email_address = ?', [$value]))
                    return 'Este email ya se encuentra registrado';
                return true;
            }, 'email', 'required'],
            'country' => ['required'],
            'password' => [function ($value) {
                if (!preg_match('/\d/', $value))
                    return 'La contraseña debe contener al menos un dígito';
                return true;
            }, 'min:6', 'required']
        ]);

        if ($validation->fails())
            $this->responseJSON($validation->errors()->toArray(), 422);

        if ($this->get('validating', false))
            $this->responseJSON(['ok']);

        $user = R::dispense('user');
        $user->name = $this->post('name');
        $user->document = $this->post('document');
        $user->email_address = $this->post('email_address');
        $user->country = $this->post('country');
        $user->password = sha1($this->post('password'));

        R::store($user);

        $this->redirect('/login');
    }

    protected function logout()
    {
        $_SESSION['user'] = null;

        $this->redirect('/login');
    }

    protected function countries()
    {
        $this->responseJSON(file_get_contents(__DIR__ . '/../../data/countries.json'));
    }

    protected function customers()
    {
        $search = $this->get('search');
        $condition = null;

        if ($search)
            $condition = 'first_name like :search or last_name like :search or email_address like :search';

        $this->render('customers', ['customers' => R::findAll('customer', $condition, [':search' => "%$search%"]), 'search' => $search]);
    }

    protected function loadCustomers()
    {
        $pool = Pool::create();

        $pool[] = async(function () {
            $loader = new CustomerData('http://www.mocky.io/v2/5d9f39263000005d005246ae?mocky-delay=10s');
            $loader->load();
        });

        $this->redirect('customers');
    }
}
