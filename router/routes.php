<?php
/***
 * En este archivo se configuran todas las rutas de la aplicación
 * y se direccionan a su respectivo controlador@accion
 */

$router = new AltoRouter();

$router->map('GET', '/', '\\App\\Controllers\\DefaultController@index');
$router->map('GET', '/users', '\\App\\Controllers\\DefaultController@users');
$router->map('GET', '/register', '\\App\\Controllers\\DefaultController@register');
$router->map('POST', '/register', '\\App\\Controllers\\DefaultController@postRegister');
$router->map('GET', '/login', '\\App\\Controllers\\DefaultController@login');
$router->map('POST', '/login', '\\App\\Controllers\\DefaultController@postLogin');
$router->map('GET', '/logout', '\\App\\Controllers\\DefaultController@logout');
$router->map('GET', '/countries', '\\App\\Controllers\\DefaultController@countries');
$router->map('GET', '/customers', '\\App\\Controllers\\DefaultController@customers');
$router->map('GET', '/load-customers', '\\App\\Controllers\\DefaultController@loadCustomers');

// match current request url
$match = $router->match();

// call closure or throw 404 status
if ($match !== false) {
    list($controller, $action) = explode('@', $match['target']);
    $user = isset($_SESSION['user']) ? $_SESSION['user'] : null;
    $validator = new Rakit\Validation\Validator();
    $controller_inst = new $controller($user, $validator);
    $controller_inst->{$action}([$match['params']]);
} else {
    // no route was matched
    header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}
