# Prueba técnica Zinobe

Sistema sencillo de directorio de clientes y usuarios

## Prerequisitos

* PHP (>7, curl habilitado), junto con un servidor web por ej Apache, Nginx, o se puede ejecutar un servidor de desarrollo local con:

    ```
    php -S localhost:8000
    ```

* MySql (>=5.7.6) o MariaDB (10) como motor de bases de datos.

* Extensiones pcntl y posix para PHP, si no se habilitan, la carga de datos del API JSON funcionará de manera sincrona.

* Composer.


### Descarga

Clonar este repositorio en la carpeta configurada para su servidor web

### Instalación

* Ejecutar el archivo SQL que se encuentra en (/database/script.sql), con el mismo, se creará la base de datos "zinobe" y el usuario "zinobe_usr"
(No es necesario generar la estructura, el ORM la genera automáticamente)

* Ejecutar composer desde la raíz del proyecto:

     ```
     composer install
     ```

## Despliegue

Ejecutar el servidor web y navegar el sitio (Debe registrar al menos un usuario)

## Desarrollado con

* [RedBeanPHP](https://github.com/gabordemooij/redbean) - ORM para PHP
* [AltoRouter](https://github.com/dannyvankooten/AltoRouter) - Routing
* [Twig](https://github.com/twigphp/Twig) - Motor de plantillas
* [rakit/validation](https://github.com/rakit/validation) - Validaciones server-side
* [spatie/async](https://github.com/spatie/async) - Ejecución asíncrona de código PHP

## Autor

* **Sergio Clavijo** - [sergioclavijo.ml](http://sergioclavijo.ml)