CREATE DATABASE IF NOT EXISTS `zinobe`;
CREATE USER IF NOT EXISTS zinobe_usr@localhost IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON zinobe.* TO zinobe_usr@localhost;